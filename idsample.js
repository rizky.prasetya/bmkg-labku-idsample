const { DateTime } = require("luxon");

const kahPeriod = 7 //7 days per period
const othPeriod = 6 //6 days per period

var startDate = DateTime.fromISO("2022-01-03");

var stationNumber = {
    "1": "Stasiun Klimatologi Aceh",
    "2": "Stasiun Meteorologi Malikussaleh",
    "3": "Balai Besar MKG Wilayah I",
    "4": "Stasiun Klimatologi Sumatera Utara",
    "5": "Stasiun Klimatologi Sumatera Selatan",
    "6": "Stasiun PAG Bukit Koto Tabang",
    "7": "Stasiun Klimatologi Sumatera Barat",
    "8": "Stasiun Klimatologi Bengkulu",
    "9": "Pos Polusi Udara Telkom Bengkulu",
    "10": "Pos Polusi Udara Universitas Bengkulu",
    "11": "Stasiun Meteorologi Sultan Syarif Kasim II",
    "12": "Stasiun Klimatologi Kampar",
    "13": "Stasiun Meteorologi Tarempa",
    "14": "Stasiun Klimatologi Jambi",
    "15": "Stasiun Meteorologi Sultan Thaha",
    "16": "Stasiun Klimatologi Lampung",
    "17": "Stasiun Meteorologi Radin Inten II",
    "18": "Stasiun Klimatologi Bangka Belitung",
    "19": "Stasiun Klimatologi Banten",
    "20": "Stasiun Geofisika Tangerang",
    "21": "Stasiun Meteorologi Maritim Serang",
    "22": "Pos Polusi Udara Ancol",
    "23": "Pos Polusi Udara Bivak",
    "24": "Pos Polusi Udara Bandengan",
    "25": "Pos Polusi Udara Glodok",
    "26": "Pos Polusi Udara Grogol",
    "27": "Pos Polusi Udara Kementan",
    "28": "Pos Polusi Udara Kemayoran",
    "29": "Pos Polusi Udara Monas",
    "30": "Pos Polusi Udara Taman Mini",
    "31": "Stasiun Geofisika Bandung",
    "32": "Stasiun Klimatologi Jawa Barat",
    "33": "Pos Polusi Udara Cibeureum",
    "34": "Stasiun Meteorologi Tunggul Wulung",
    "35": "Stasiun Klimatologi Jawa Tengah",
    "36": "Stasiun Meteorologi Maritim Tegal",
    "37": "Stasiun Geofisika Sleman",
    "38": "Stasiun Klimatologi DI Yogyakarta",
    "39": "Stasiun Meteorologi Juanda",
    "40": "Stasiun Klimatologi Jawa Timur",
    "41": "Stasiun Klimatologi Kalimantan Barat",
    "42": "Stasiun Meteorologi Supadio",
    "43": "Stasiun Meteorologi Temindung",
    "44": "Stasiun Meteorologi SAMS Sepinggan",
    "45": "Stasiun Meteorologi Kalimantan Selatan",
    "46": "Stasiun Meteorologi Tjilik Riwut",
    "47": "Stasiun Klimatologi Bali",
    "48": "Stasiun Meteorologi I Gusti Ngurah Rai",
    "49": "Stasiun Meteorologi Eltari",
    "50": "Stasiun Klimatologi Nusa Tenggara Timur",
    "51": "Stasiun Meteorologi Zainuddin Abdul Madjid",
    "52": "Stasiun Klimatologi Nusa Tenggara Barat",
    "53": "Stasiun Meteorologi Djalaluddin",
    "54": "Stasiun Klimatologi Gorontalo",
    "55": "Stasiun Meteorologi Majene",
    "56": "Stasiun Meteorologi Mutiara Sis-Al Jufri",
    "57": "Stasiun PAG Lore Lindu Bariri",
    "58": "Stasiun Klimatologi Sulawesi Utara",
    "59": "Stasiun Meteorologi Sam Ratulangi",
    "60": "Stasiun Geofisika Manado",
    "61": "Stasiun Meteorologi Beto Ambari",
    "62": "Stasiun Klimatologi Sulawesi Tenggara",
    "63": "Balai Besar MKG Wilayah IV",
    "64": "Stasiun Klimatologi Sulawesi Selatan",
    "65": "Stasiun Klimatologi Maluku",
    "66": "Stasiun Meteorologi Domine Eduard Osok",
    "67": "Stasiun PAG Puncak Vihara Klademak",
    "68": "Stasiun Meteorologi Frans Kaisiepo",
    "69": "Stasiun Klimatologi Jayapura",
    "70": "Stasiun Klimatologi Merauke",
    "71": "Stasiun Geofisika Jayapura"
}

var stationCode = {
    "1": "ACB",
    "2": "MSH",
    "3": "BW1",
    "4": "DES",
    "5": "PLB",
    "6": "BKT",
    "7": "PAR",
    "8": "BKL",
    "9": "TEL",
    "10": "UNB",
    "11": "SSK",
    "12": "TMG",
    "13": "TRP",
    "14": "JMB",
    "15": "STH",
    "16": "PSW",
    "17": "RDI",
    "18": "KBA",
    "19": "TAS",
    "20": "TNG",
    "21": "SRG",
    "22": "ACL",
    "23": "BVK",
    "24": "DLT",
    "25": "GDK",
    "26": "GRG",
    "27": "KMT",
    "28": "KMY",
    "29": "MNS",
    "30": "TMI",
    "31": "BDG",
    "32": "BGR",
    "33": "CBR",
    "34": "CLP",
    "35": "SMG",
    "36": "TGL",
    "37": "JOG",
    "38": "MLT",
    "39": "JUD",
    "40": "MLG",
    "41": "MPH",
    "42": "SPD",
    "43": "TMD",
    "44": "SPG",
    "45": "BJB",
    "46": "TJR",
    "47": "JBR",
    "48": "NGU",
    "49": "ETR",
    "50": "KUP",
    "51": "BIL",
    "52": "LOM",
    "53": "JLD",
    "54": "TKB",
    "55": "MJE",
    "56": "MTR",
    "57": "PSO",
    "58": "MHU",
    "59": "SRT",
    "60": "WNG",
    "61": "BBU",
    "62": "RMT",
    "63": "BW4",
    "64": "MRS",
    "65": "SER",
    "66": "SGN",
    "67": "SOR",
    "68": "FKS",
    "69": "JAP",
    "70": "MRK",
    "71": "JAY"
}

// Asumsi Stasiun Number
var num = "64";

// Asumsi Form KAH, untuk selain KAH isi "Others"
var tipe = "KAH";
// var tipe = "Others";

// Asumsikan tahun
var tahun = 2020;

// Asumsikan Periode
var periode = 2;

// Asumsikan sekuens baru
var sekuens = 5;

if (tipe == "KAH") {
    mul = kahPeriod;
} else if (tipe == "Others") {
    mul = othPeriod;
} else {
    alert("Tipe harus diisi benar");
    exit()
}

if (tipe == "KAH") {
    selisih = 7;
} else if (tipe == "Others") {
    selisih = 1;
} else {
    alert("Tipe harus diisi benar");
    exit();
}

if (tahun == 2022) {
    pasang = startDate.plus({ days: (periode - 1) * mul });
} else if (tahun > 2022) {
    if (tipe == "KAH") {
        sKAH = DateTime.local(tahun, 1, 1).startOf('week');
        // if the Monday of the first week of the year is in previous year
        if (sKAH.year == tahun - 1) {
            sKAH = DateTime.local(tahun, 1, 7).startOf('week');
        }
        // Start from the first Monday
        startDate = sKAH;
        pasang = startDate.plus({ days: (periode - 1) * mul });
    }
    if (tipe == "Others") {
        // Find startDate in year
        sOther = startDate.plus({ days: (tahun - 2022) * mul * 61 });

        // Check if sOther falls within the first 6 days of the year
        d = sOther.diff(DateTime.local(tahun, 1, 1), "days").toObject().days;

        if (d < 6) {
            startDate = sOther;
        } else {
            startDate = sOther.minus(Math.floor(d / 6) * mul);
        };
        pasang = startDate.plus({ days: (periode - 1) * mul });
    }
} else if (tahun < 2022) {
    if (tipe == "KAH") {
        sKAH = DateTime.local(tahun, 1, 1).startOf('week');
        // if the Monday of the first week of the year is in previous year
        if (sKAH.year == tahun - 1) {
            sKAH = DateTime.local(tahun, 1, 7).startOf('week');
        }
        // Start from the first Monday
        startDate = sKAH;
        pasang = startDate.plus({ days: (periode - 1) * mul });
    }
    if (tipe == "Others") {
        // Find startDate in year
        sOther = startDate.plus({ days: (tahun - 2022) * mul * 61 });

        // Check if sOther falls within the first 6 days of the year
        d = sOther.diff(DateTime.local(tahun, 1, 1), "days").toObject().days;

        if (d < 6) {
            startDate = sOther;
        } else {
            startDate = sOther.minus(Math.floor(d / 6) * mul);
        };
        pasang = startDate.plus({ days: (periode - 1) * mul });
    }
}

periode = periode.toString().padStart(2, 0);
sekuens = sekuens.toString().padStart(2, 0);
console.log("Type: " + tipe);
console.log("Tanggal pasang awal tahun: " + startDate.toISO());
console.log("Periode: " + periode);
console.log("Tanggal pasang untuk periode: " + pasang.toISO());
console.log("Tanggal angkat untuk periode: " + pasang.plus({ days: selisih }).toISO());
console.log("IDSAMPEL: " + stationCode[num] + tahun.toString().substring(2, 4) + pasang.toFormat("LL") + periode + sekuens);
